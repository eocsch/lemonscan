#!/usr/bin/env python
#-*-coding:utf-8-*-
import requests
import json
#http_addrs = 'http://47.105.111.1:8898'
http_addrs = 'http://127.0.0.1:8898'
#测试全网基本信息
def testBaseInfo(db_name,node_address='47.105.111.1:8888'):
    posturls = http_addrs + '/v1/eos/get_base_info'
    payload = {'db_name':db_name,'coin_symbol':'EOC','node_address':node_address}
    header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    print('get response')
    print(response)
    res_json = response.json()
    print(res_json)
    pass

#测试最新区块列表
def testBlockInfo(db_name):
    posturls = http_addrs + '/v1/eos/get_block_info'
    payload = {'db_name':db_name,'page_num':1,'page_size':30}
    header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    res_json = response.json()
    print(res_json)
    pass

#测试超级节点列表
def testBpInfo(db_name):
    posturls = http_addrs + '/v1/eos/get_block_producers_info'
    payload = {'db_name':db_name,'page_num':1,'page_size':30}
    header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    print('post success!')
    res_json = response.json()
    print(res_json)
    pass

#测试区块详情
def testBlockDetail(db_name,block_num):
    posturls = http_addrs + '/v1/eos/get_block_detail'
    payload = {'db_name':db_name,"block_num":block_num}
    header = {'Content-Type': 'application/json'}
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    print('post success!')
    res_json = response.json()
    print(res_json)
    pass

def testTransactionDetail(db_name, transaction_id):
    posturls = http_addrs + '/v1/eos/get_transaction_detail'
    payload = {'db_name':db_name,"transaction_id":transaction_id}
    header = {'Content-Type': 'application/json'}
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    print('post success!')
    res_json = response.json()
    print(res_json)
    pass

def testGetSearch(db_name, content, node_addr='47.105.111.1:5992'):
    posturls = http_addrs + '/v1/eos/get_search'
    payload = {'db_name':db_name,"content":content,'node_address':node_addr}
    header = {'Content-Type': 'application/json'}
    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    print('post success!')
    res_json = response.json()
    print(res_json)
    pass

if __name__ == '__main__':
    #testBaseInfo('EOCS','47.105.111.1:5992')
    #testBaseInfo('EOC')
    #testBlockInfo('EOCS')
    #testBpInfo('EOCS')
    #testBlockDetail('EOCS',313251)
    #testGetSearch('EOCS','48257f1bb86e27b64ab79dca9525c9c3be822ca90c4eacd3c6681db2ae9746f8')
    testGetSearch('EOCS','lemon')
                            
    #testGetSearch('EOCS',313251)
    pass