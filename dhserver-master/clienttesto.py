#-*-coding:utf-8-*-
import requests
import json
from urllib.parse import urlencode

urlcode = 'http://127.0.0.1:8898'
content = {'account':'shanliang', 'contract':'tcoin', 'symbol':'TKT'}
urlcode1 = 'http://127.0.0.1:8898/v1/eos/accounts/inita2345/balance?' + urlencode(content)
import os, sys
from enum import Enum, unique

@unique
class key_enum(Enum):
    VOTES = 1
    OWNER = 2

keyy = key_enum['VOTES']
print(key_enum['VOTES'].value)
#payload = {'name': 'wuchen', 'password': '123456', 'key':keyy}
#payload = {'key':0, 'start':'2', 'end':'3', 'limit':4}
def requestwrapper(requestfunc):
    def wrapperrequest(*args,**kwargs):
        ret=requestfunc(*args,**kwargs)
        if ret:
            print(ret.url)
            #print(ret.text)
        return ret
    return wrapperrequest

#posturls='http://127.0.0.1:8898/login'
#posturls='http://127.0.0.1:8898/v1/eos/producers'

@requestwrapper
def getrequest(urls=''):
    ret=requests.get(urls,timeout=5)
    return ret
@requestwrapper
def postrequest(urls='', datas={}):
    ret=requests.post(urls,data=datas,timeout=5)
    return ret

@requestwrapper
def postjsonrequest(urls='',datas={'key':0, 'start':2, 'end':3, 'limit':4}):
    header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
    response = requests.post(urls, headers=header, data=json.dumps(datas),timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    return response

@requestwrapper
def postfilerequest(urls='',datas={},filepath=''):
    file = {"files" : open(filepath, "rb") }
    response = requests.post(urls, files=file , data=datas,timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
    return response

@requestwrapper
def downfilerequest(urls='',datas={},filename=''):
    datas['filename'] = filename
    r = requests.post(urls,data=datas,timeout=15)
    download_path = os.path.join(os.path.dirname(__file__), 'download')
    if(os.path.exists(download_path)==False):
        os.makedirs(download_path)
    filename = os.path.join(download_path,filename)
    if(os.path.exists(filename)):
        return None
    with open(filename,'wb') as f:
        f.write(r.content)
    return r
    

    
if __name__ == "__main__":
    try:
            
        posturls='http://127.0.0.1:8898/v1/eos/get_block_detail'
        
        posturls='http://47.105.111.1:8898/v1/eos/get_search'
        posturls='http://127.0.0.1:8898/v1/eos/get_base_info'
        posturls='http://127.0.0.1:8898/v1/eos/get_block_detail'
        posturls='http://127.0.0.1:8898/v1/eos/get_transaction_detail'
        posturls='http://127.0.0.1:8898/v1/eos/get_search'
        posturls='http://127.0.0.1:8898/v1/eos/get_block_info'
        posturls='http://127.0.0.1:8898/v1/eos/get_block_detail'
        
        posturls='http://47.105.111.1:8898/v1/eos/get_block_info'
        posturls='http://47.105.111.1:8898/v1/eos/get_block_detail'
        posturls='http://47.105.111.1:8898/v1/eos/get_transaction_detail'
        posturls='http://47.105.111.1:8898/v1/eos/get_search'
        posturls='http://47.105.111.1:8898/v1/eos/get_cross_chain_action_info'
      
        payload = {'id': '06f5f064a4782dfe684f103fd19086f769fdaef9456f0b68552ddb757e31c6ba', 'block_num': 690, 'block_num_start': 690, 'block_num_end':695, 'num_asc': True, 'include_transactions': True}
        payload = {'transaction_id': '12b51e6f7540e62409f3bd44d01d9924aaf04b8d46f9ea520286b4199e5a21f2', 'block_num': 690, 'block_num_start': 690, 'block_num_end':695, 'num_asc': True, 'include_transactions': True}
       
        payload = {'page_num':1, 'page_size':4, 'content':0, 'block_num':690}
        payload = {'transaction_id': '12b51e6f7540e62409f3bd44d01d9924aaf04b8d46f9ea520286b4199e5a21f2', 'block_num': 690, 'block_num_start': 690, 'block_num_end':695, 'num_asc': True, 'include_transactions': True}
        payload = {'page_num':1, 'page_size':4, 'content':'01b59ca7874ad0fc85a6c9e90674696f9905c4261af8e7df38c646283b0cafed', 'block_num':690}
        postjsonrequest(posturls, payload)
       
    except requests.exceptions.ReadTimeout:
        print('request time out...')