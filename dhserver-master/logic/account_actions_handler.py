#-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import ast
import requests
import re

from odm_class import blocks, transactions, accounts, action_traces, cross_chain_tx_actions, disconnect_db, connect_db


class AccountActionsHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("AccountActionsHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))

    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login account_actions success!!!")
            print(data)
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request",
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return

            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            account_name = ''
            if 'account_name' in data.keys():
                account_name = data['account_name']
            #返回请求参数错误的响应
            if account_name == None\
               or (type(account_name).__name__ != 'str')\
               or (type(account_name).__name__ == 'str' and len(account_name) < 1)\
               or (type(account_name).__name__ == 'str' and len(account_name) > 12):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return
            matchObj = re.match( r'([a-z]|[1-5]|[.]){1,12}', account_name)
            if matchObj:
                print('account')
            else:
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return
            account_record = accounts.objects(name = account_name).first()
            data_info = {}
            if account_record == None:
                respon = {
                    "errno": 100003, 
                    "errmsg": "Account Not Exist", 
                    "type": "account",
                    "data": None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return
            #获取请求信息中的参数：区块信息显示的页数page_num
            page_num = 0
            if 'page_num' in data.keys():
                page_num = data['page_num']
            #返回请求参数错误的响应：page_num不是大于0的int型数
            if page_num == None or type(page_num).__name__ != 'int' or (type(page_num).__name__ == 'int' and page_num < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_num error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return
            #获取请求信息中的参数：每页显示的区块个数page_size
            page_size = 0
            if 'page_size' in data.keys():
                page_size = data['page_size']
            #返回请求参数错误的响应：page_size不是大于0的int型数
            if page_size == None or type(page_size).__name__ != 'int' or (type(page_size).__name__ == 'int' and page_size < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_size error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                print(respon_json)
                self.write(respon_json)
                self.finish()
                return

            action_type = ''
            if 'action_type' in data.keys():
                action_type = data['action_type']
            actions_search = None
            actions_count = 0
            if action_type == "transfer":
                actions_search = action_traces.objects(__raw__={"$and":[{"act.name":action_type},{"receipt.receiver":"eosio.token"}, {"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}]}).order_by('-createdAt').limit(page_size).skip((page_num - 1) * page_size)
                #actions_search = action_traces.objects(__raw__={"$and":[{"act.name":"tkt111111111"}, {"$or":[{"act.data.from":"tkt111111111"},{"act.data.to":"tkt111111111"}]}]}).order_by('-createdAt')
                actions_count = action_traces.objects(__raw__={"$and":[{"act.name":action_type},{"receipt.receiver":"eosio.token"}, {"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}]}).count()
            else:
               # actions_count = action_traces.objects(__raw__={"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}).count()
                #返回所有的文档对象列表
               # actions_search = action_traces.objects(__raw__={"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}).order_by('-createdAt').limit(page_size).skip((page_num - 1) * page_size)
            
               actions_search = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}]}).order_by('-createdAt')
               actions_count = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"act.data.from":account_name},{"act.data.to":account_name}]}]}).count()
  
            #没有查询到actions，返回100005的错误码
            if actions_search.first() == None:
                respon = {
                    "errno": 100005,
                    "errmsg": "Action Not Exist",
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
 #               print(respon_json)
                self.write(respon_json)
                self.finish()
                return
            actions = []
            for action in actions_search:
                action_add = action['act']
                action_add.update({'block_num': action['block_num'], 'block_time': action['block_time']})
                actions.append(action_add)
            #返回查询到的区块信息
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": {
                    'actions_count': actions_count,
                    "actions_info": actions
                }
            }
            respon_json = tornado.escape.json_encode(respon)
#            print(respon_json)
            self.write(respon_json)
            self.finish()

        except Exception as e:
            print(str(e))
            print('get_argument account_actions_handler_ exception...')

    get = post

