#-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import ast
import requests
import configparser

from odm_class import blocks, transactions, accounts, action_traces, cross_chain_tx_actions, disconnect_db, connect_db

class BaseInfoHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("BaseInfoHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))

    def post(self):
        try:
            #Base.metadata.create_all(engine)
            #session = Session()
            data=json.loads(self.request.body.decode('utf-8'))
            print("call baseinfo handler success!")
            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            coin_symbol = 'EOC'
            if 'coin_symbol' in data.keys():
                coin_symbol = data['coin_symbol']

            address = '47.105.111.1:8888'
            if 'node_address' in data.keys():
                address = data['node_address']

            #data=json.loads(self.request.body.decode('utf-8'))
            
            head_block = blocks.objects.all().order_by('-block_num').first()

            #head_block = session.query(Block).order_by(Block.block_num.desc()).first()
            #最新区块的num
            head_block_num = head_block.block_num
            #所有的交易数
            total_transaction_num = transactions.objects.all().count()
            #所有的action数
            total_action_num = action_traces.objects.all().count()
            #所有的账户数
            total_account_num = accounts.objects.all().count()
            
            #posturls = "http://39.108.228.174:9992/v1/chain/get_info"
            print(address)
            posturls = "http://" + address + "/v1/chain/get_info"
            header = {'Connection': 'close', 'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
            
            response = requests.post(posturls, headers=header, timeout=5)
            #print("..........................**")
            ress = response.json()
            
            print(ress)
            response.close()
           
            #EOC服务器版本
            server_version = 'e87d245d'
            if 'server_version' in ress:
                server_version = ress['server_version']
            #链id
            chain_id = 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f'
            if 'chain_id' in ress:
                chain_id = ress['chain_id']
            #发行量
            posturls = "http://" + address + "/v1/chain/get_currency_stats"
            payload = {'scope':'eosio', 'code':'eosio.token', 'symbol':coin_symbol, 'table':'global', 'json':True}
            header = {'Connection': 'close', 'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
            #header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
            response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
            ress = response.json()
            response.close()
            supply = '0.0000 EOC'
            supply_float = 0.0
            if 'EOS' in ress:
                supply = ress['EOS']['supply'].split(' ')[0] + ' EOS'
                supply_float = float(ress['EOS']['supply'].split(' ')[0])
            elif 'EOC' in ress:
                supply = ress['EOC']['supply']
                supply_float = float(ress['EOC']['supply'].split(' ')[0])

            total_activated_stake = 0
            #已投票EOC
            #eocs_voted = total_activated_stake/10000
            eocs_voted = '368900000 EOC'
            posturls = 'http://' + address + '/v1/chain/get_table_rows'
            payload = {'scope':'eosio', 'code':'eosio', 'symbol':'EOC', 'table':'global', 'json':True}
            header = {'Connection': 'close', 'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
            response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
            ress = response.json()
            response.close()

            eocs_voted_float = 0.0
            if ress != None and 'rows' in ress:
                eocs_voted_float = float(ress['rows'][0]['total_activated_stake'])/10000
            eocs_voted = "%.04f EOC" % eocs_voted_float

            #投票率
            eocs_voted_rate = '36.89%'
            if supply_float != 0 and  eocs_voted_float/supply_float < 0.0001:
                eocs_voted_rate = '< 0.01%'
            elif supply_float != 0:
                eocs_voted_rate = '{:.2%}'.format(eocs_voted_float/supply_float)    #percent:%
            #总内存
            max_ram_size = '68719476736'
            if 'rows' in ress:
                max_ram_size = ress['rows'][0]['max_ram_size']

            #已分配内存
            total_ram_bytes_reserved = '43465069035.52'
            if 'rows' in ress:
                total_ram_bytes_reserved = ress['rows'][0]['total_ram_bytes_reserved']
            #分配内存比
            ram_rate = ''
            if float(max_ram_size) != 0 :
                ram_rate = '{:.2%}'.format(float(total_ram_bytes_reserved)/float(max_ram_size))
            #跨链交易操作数
            cross_chain_action_num = cross_chain_tx_actions.objects().all().count()
            #返回的响应信息
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": {
                    'server_version': server_version,
                    'chain_id': chain_id,
                    'head_block_num': head_block_num,
                    'total_transaction_num': total_transaction_num,
                    'total_action_num': total_action_num,
                    'total_account_num': total_account_num,
                    'supply': supply.split(' ')[0],
                    'eocs_voted': eocs_voted.split(' ')[0],
                    'eocs_voted_rate': eocs_voted_rate,
                    'max_ram_size': max_ram_size,
                    'total_ram_bytes_reserved': total_ram_bytes_reserved,
                    'ram_rate': ram_rate,
                    'cross_chain_action_num': cross_chain_action_num
                }
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print('get_argument base_info_handler exception...')
            print(str(e))

    get = post
