#-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import ast

from odm_class import blocks
from odm_class import disconnect_db
from odm_class import connect_db

class BlockInfoHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("BlockInfoHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))
  
    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login block_info_handler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            #获取请求信息中的参数：区块信息显示的页数page_num
            page_num = 0
            if 'page_num' in data.keys():
                page_num = data['page_num']
           
            #返回请求参数错误的响应：page_num不是大于0的int型数
            if page_num == None or type(page_num).__name__ != 'int' or (type(page_num).__name__ == 'int' and page_num < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_num error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

          
            #获取请求信息中的参数：每页显示的区块个数page_size
            page_size = 0
            if 'page_size' in data.keys():
                page_size = data['page_size']
            #返回请求参数错误的响应：page_size不是大于0的int型数
            if page_size == None or type(page_size).__name__ != 'int' or (type(page_size).__name__ == 'int' and page_size < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_size error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #total = session.query(Block).order_by(Block.block_num.desc()).count()
            #blocks = session.query(Block).order_by(Block.block_num.desc()).limit(page_size).offset((page_num - 1) * page_size)
            total = blocks.objects.all().count()
            blocks_search = blocks.objects.all().order_by('-block_num').limit(page_size).skip((page_num - 1) * page_size) #返回所有的文档对象列表
            #没有查询到区块，返回100001的错误码
            if blocks_search.first() == None:
                respon = {
                    "errno": 100001, 
                    "errmsg": "Block Not Exist", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            block_info = []
            for block in blocks_search:
                block_dict = block.block
                info = {
                    "block_num": block.block_num, 
                    "block_id": block.block_id,
                    "trx_num": len(block_dict['transactions']), 
                    #"action_num": 0,#need
                    "producer": block_dict['producer'], 
                    "timestamp": block_dict['timestamp']
                }
                block_info.append(info)
            #返回查询到的区块信息
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": {
                    'total': total,
                    "block_info": block_info
                }
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print('get_argument block_info_handler exception...')

    get = post
