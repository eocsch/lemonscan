#-*-coding:utf-8-*-
import requests
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
#from config import settings
from enum import Enum
from odm_class import blocks, producers, disconnect_db, connect_db


class ProducersHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("ProducersHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))
    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login producers_handler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            #获取请求信息中的参数：区块信息显示的页数page_num
            page_num = 0
            if 'page_num' in data.keys():
                page_num = data['page_num']
            #返回请求参数错误的响应：page_num不是大于0的int型数
            if page_num == None or type(page_num).__name__ != 'int' or (type(page_num).__name__ == 'int' and page_num < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_num error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #获取请求信息中的参数：每页显示的区块个数page_size
            page_size = 0
            if 'page_size' in data.keys():
                page_size = data['page_size']
            #返回请求参数错误的响应：page_size不是大于0的int型数
            if page_size == None or type(page_size).__name__ != 'int' or (type(page_size).__name__ == 'int' and page_size < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_size error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            rows = []
            start_producer = (page_num - 1) * page_size + 1
            now_producer = start_producer
            producers_count = producers.objects().all().count()
            total_producer_vote = 0
            if producers_count > 0:
                total_producer_vote = producers.objects().first().total_producer_vote_weight
            while now_producer < (producers_count + 1) and now_producer < (start_producer + page_size):
                producer_db = producers.objects(ranking = now_producer).first()
                if producer_db == None:
                    continue
                producer_info = {
                    "ranking": producer_db.ranking,
                    "account_name": producer_db.account_name,
                    "is_active": producer_db.is_active,
                    "location": producer_db.location,
                    "blocks_count": producer_db.blocks_count,
                    "vote_rate": producer_db.vote_rate,
                    "vote": producer_db.vote,
                    "reward": producer_db.reward,
                    "url": producer_db.url
                }
                rows.append(producer_info)
                now_producer += 1
            #返回查询到的生产者信息
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": {
                    "total_vote": total_producer_vote,
                    "total": producers_count,
                    "producers_info": rows
                }
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print('ProducersHandler exception...')
            print(str(e))
    get = post
