 #-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
from odm_class import transactions, transaction_traces, disconnect_db, connect_db


class TransactionDetailHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("TransactionDetailHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))

    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login transaction_detail_handler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'tx': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            
            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            transaction_id = ''
            #获取请求的参数：交易id
            if 'transaction_id' in data.keys():
                transaction_id = data['transaction_id']

            #返回请求参数错误的响应：不是64位长度的字符串
            if transaction_id == None or type(transaction_id).__name__ != 'str' or (type(transaction_id).__name__ == 'str' and len(transaction_id) != 64):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'tx': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            ref_block_num = 0
            status = ''
            cpu_usage_us = 0
            net_usage_words = 0
            ref_block_num = 0
            expiration = ''
            actions = []
            #根据请求的参数（交易id）在表中查询交易的信息
            #transaction_info = session.query(Transaction).filter(Transaction.trx_id == transaction_id).first()
            transaction_info = transactions.objects(trx_id = transaction_id).first()
            if transaction_info != None:
                expiration = transaction_info.expiration
                ref_block_num = transaction_info.ref_block_num
                actions = transaction_info.actions
            #根据请求的参数（交易id）在表中查询交易trace的信息
            #trx_trace_info = session.query(TransactionTrace).filter(TransactionTrace.id == transaction_id).first()
            trx_trace_info = transaction_traces.objects(id = transaction_id).first()
            if trx_trace_info != None:
                dict_receipt = trx_trace_info.receipt
                status = dict_receipt['status']
                cpu_usage_us = dict_receipt['cpu_usage_us']
                net_usage_words = dict_receipt['net_usage_words']
            #没有查询到交易信息，返回100002的错误码
            if transaction_info == None and trx_trace_info == None:
                respon = {
                    "errno": 100002, 
                    "errmsg": "Transaction Not Exist", 
                    'tx': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #交易信息
            info = {
                "id": transaction_id,
                "status": status,
                "cpu_usage_us": cpu_usage_us,
                "net_usage_words": net_usage_words,
                "ref_block_num": ref_block_num,
                "expiration": expiration,
                "actions": actions
            }
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                'tx':info
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print('execute transaction_detail_handler exception...')
    get = post