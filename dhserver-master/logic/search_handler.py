#-*-coding:utf-8-*-
import requests
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import re
from odm_class import transactions, transaction_traces, blocks, accounts, pub_keys, action_traces, disconnect_db, connect_db


class SearchlHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("SearchlHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))
    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login search_handler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request",
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']
            
            address = '47.105.111.1:5992'
            if 'node_address' in data.keys():
                address = data['node_address']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            content = ''
            if 'content' in data.keys():
                content = data['content']
            #返回请求参数错误的响应
            if content == None\
               or (type(content).__name__ != 'str' and type(content).__name__ != 'int')\
               or (type(content).__name__ == 'str' and len(content) < 1)\
               or (type(content).__name__ == 'int' and content < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            if type(content).__name__ == 'str' and content.isdigit():
                content = int(content)
            #请求参数为int类型，即通过区块num查询区块信息
            if type(content).__name__ == 'int':
                block_info = blocks.objects(block_num = content).first()
                #没有查询到区块信息，返回100001的错误码
                if block_info == None:
                    respon = {
                        "errno": 100001, 
                        "errmsg": "Block Not Exist", 
                        "type": "block",
                        'data': None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                block_dict = block_info.block
                info = {
                    "block_num": content, 
                    "confirmed": block_dict['confirmed'],
                    "id": block_info.block_id, 
                    "new_producers": block_dict['new_producers'],
                    "previous": block_dict['previous'],
                    "producer": block_dict['producer'],
                    "producer_signature": block_dict['producer_signature'],
                    "timestamp": block_dict['timestamp'],
                    "transaction_mroot": block_dict['transaction_mroot'],
                    "action_mroot": block_dict['action_mroot'],
                    "transactions": block_dict['transactions']
                }
                respon = {
                    "errno": 0, 
                    "errmsg": "Success",
                    "type": "block", 
                    "data": {
                        "pub_key": None,
                        "account": None,
                        "transaction": None,
                        "block": info
                    }
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
            #请求参数为64位长度的字符串，即通过交易id查询交易信息
            elif len(content) == 64:
                ref_block_num = 0
                status = ''
                cpu_usage_us = 0
                net_usage_words = 0
                ref_block_num = 0
                expiration = ''
                actions = []
                #根据请求的参数（交易id）在表中查询交易的信息
                #transaction_info = session.query(Transaction).filter(Transaction.trx_id == transaction_id).first()
                transaction_info = transactions.objects(trx_id = content).first()
                if transaction_info != None:
                    expiration = transaction_info.expiration
                    ref_block_num = transaction_info.ref_block_num
                    actions = transaction_info.actions
                #根据请求的参数（交易id）在表中查询交易trace的信息
                #trx_trace_info = session.query(TransactionTrace).filter(TransactionTrace.id == transaction_id).first()
                trx_trace_info = transaction_traces.objects(id = content).first()
                if trx_trace_info != None:
                    dict_receipt = trx_trace_info.receipt
                    status = dict_receipt['status']
                    cpu_usage_us = dict_receipt['cpu_usage_us']
                    net_usage_words = dict_receipt['net_usage_words']
                #没有查询到交易信息，返回100002的错误码
                if transaction_info == None and trx_trace_info == None:
                    respon = {
                        "errno": 100002, 
                        "errmsg": "Transaction Not Exist", 
                        "type": "transaction", 
                        'data': None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                info = {
                    "id": content,
                    "status": status,
                    "cpu_usage_us": cpu_usage_us,
                    "net_usage_words": net_usage_words,
                    "ref_block_num": ref_block_num,
                    "expiration": expiration,
                    "actions": actions
                }
                #交易信息
                respon = {
                    "errno": 0, 
                    "errmsg": "Success",
                    "type": "transaction", 
                    "data": {
                        "pub_key": None,
                        "account": None,
                        "transaction": info,
                        "block": None
                    }
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
            #请求参数为53位长度的字符串，即通过公钥查询公钥信息
            elif len(content) == 53:
                #public_key_info = session.query(PublicKey).filter(PublicKey.public_key == content)
                public_key_info = pub_keys.objects(public_key = content)
                #没有查询到公钥信息，返回100004的错误码
                if public_key_info.first() == None:
                    respon = {
                        "errno": 100004, 
                        "errmsg": "PublicKey NOt Exist", 
                        "type": "pub_key",
                        "data": None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                data_info = []
                for each_info in public_key_info:
                    account = each_info.account
                    permission = each_info.permission
                    public_key = each_info.public_key
                    createdAt = str(each_info.createdAt)
                    pub_key_add = {
                        "account" : account, 
                        "permission" : permission, 
                        "public_key" : public_key, 
                        "createdAt" : createdAt
                    }
                    data_info.append(pub_key_add)
                respon = {
                    "errno": 0,
                    "errmsg": "Success",
                    "type": "pub_key",
                    "data": {
                        "pub_key": data_info,
                        "account": None,
                        "transaction": None,
                        "block": None
                    }
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
            #通过账户名查询账户信息
            elif len(content) < 13:
                matchObj = re.match( r'([a-z]|[1-5]|[.]){1,12}', content)
                account_record = accounts.objects(name = content).first()
                data_info = {}
                if account_record == None:
                    respon = {
                        "errno": 100003, 
                        "errmsg": "Account Not Exist", 
                        "type": "account",
                        "data": None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                else:
                    posturls = 'http://'+ address+ '/v1/chain/get_account'
                    payload = {'account_name':content, 'code':'eosio', 'table':'global', 'json':True}
                    header = {'Connection': 'close', 'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
                    response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
                    res_json = response.json()
                    print('aaa')
                    print(res_json)
                    print('account')
                    response.close()
                    liquid_balance = "0.0000 EOC"
                    if "core_liquid_balance" in res_json:
                        liquid_balance = res_json['core_liquid_balance'].split(' ')[0] + ' EOC'
                    cpu_staked =  "0.0000 EOC"
                    net_staked =  "0.0000 EOC"
                    if res_json['self_delegated_bandwidth'] != None:
                        cpu_staked = res_json['self_delegated_bandwidth']['cpu_weight'].split(' ')[0] + ' EOC'
                        net_staked = res_json['self_delegated_bandwidth']['net_weight'].split(' ')[0] + ' EOC'
                    ram_quota = str(res_json['ram_quota'])
                    ram_used = str(res_json['ram_usage'])
                    cpu_available = '0'
                    cpu_used = '0'
                    net_available = '0'
                    net_used = '0'
                    if 'cpu_limit' in res_json:
                        cpu_available = res_json['cpu_limit']['available']
                        cpu_used = str(res_json['cpu_limit']['used'])
                    if 'net_limit' in res_json:
                        net_available = res_json['net_limit']['available']
                        net_used = str(res_json['net_limit']['used'])
                   
                    actions = []
                    #trx_search = action_traces.objects(__raw__={'act.data.from': content}).order_by('-createdAt')
                    #actions_count = action_traces.objects(__raw__={'act.data.from': content}).count()
                    action_type = ''
                    print('data is: ', data)
                    print("data.keys",data.keys())
                    if 'action_type' in data.keys():
                        action_type = data['action_type']
                        print("action_type: ", action_type)
                    trx_search = None
                    actions_count = 0
                    if action_type == "transfer":
                        trx_search = action_traces.objects(__raw__={"$and":[{"act.name":action_type},{"receipt.receiver":"eosio.token"}, {"$or":[{"act.data.from":content},{"act.data.to":content}]}]}).order_by('-createdAt')
                        actions_count = action_traces.objects(__raw__={"$and":[{"act.name":action_type}, {"receipt.receiver":"eosio.token"},{"$or":[{"act.data.from":content},{"act.data.to":content}]}]}).count()
                    else:
                        #trx_search = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"actions.data.from":content},{"actions.data.to":content}]}]}).order_by('-createdAt')
                        #actions_count = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"actions.data.from":content},{"actions.data.to":content}]}]}).count()
                         
                        trx_search = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"act.data.from":content},{"act.data.to":content}]}]}).order_by('-createdAt')
                        actions_count = action_traces.objects(__raw__={"$and":[{"receipt.receiver":"eosio.token"},{"$or":[{"act.data.from":content},{"act.data.to":content}]}]}).count()
                        #print(trx_search.explain())
                    print('actions_count:')
                    print(actions_count)
                    act_count = 0
                    for trx in trx_search:
                        if act_count < 20:
                            action_add = trx['act']
                            action_add.update({'block_num': trx['block_num'], 'block_time': trx['block_time']})
                            actions.append(action_add)
                            act_count = act_count + 1
                    #print(actions)
                    data_info = {
                        "name": content,
                        "total_balance": "0.0000 EOC",
                        "liquid_balance": liquid_balance,
                        "cpu_staked": cpu_staked,
                        "net_staked": net_staked,
                        "ram_quota": ram_quota,
                        "ram_used": ram_used,
                        "cpu_available": cpu_available,
                        "cpu_used": cpu_used,
                        "net_available": net_available, 
                        "net_used": net_used,
                        "actions": actions,
                        "actions_count": actions_count
                    }
                respon = {
                    "errno": 0,
                    "errmsg": "Success",
                    "type": "account",
                    "data": {
                        "pub_key": None,
                        "account": data_info,
                        "transaction": None,
                        "block": None
                    }
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
            else:
                #返回请求参数错误的响应(不是符合上面4种查询条件的参数)
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    "type": None,
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
        except Exception as e:
            print('get_argument SearchHandler exception...')
            print(str(e))
    get = post
