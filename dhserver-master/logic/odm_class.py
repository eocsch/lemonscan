# coding=utf-8

from mongoengine import *
import datetime
mysql_addr = '47.105.111.1'
mysql_port = 3306
mysql_username = 'dhome'
mysql_dbname = 'dhomedb'
mysql_pswd = '123456'

mongo_addr = '47.105.111.1'
mongo_port = 27017

def connect_db(dbstr):
    connect(dbstr,host=mongo_addr, port=mongo_port, alias='db1')

def disconnect_db():
    disconnect(alias='db1')
    pass

#区块表
class blocks(Document):
    block_id = StringField(required=True, max_length=64)
    block = BinaryField(required=True)
    block_num = IntField(required=True)
    createdAt = DateTimeField(required=True)
    irreversible = BooleanField()
    validated = BooleanField()
    in_current_chain = BooleanField()
    updatedAt = DateTimeField()
    meta = {'db_alias': 'db1'}


#账户表
class accounts(Document):
    name = StringField(required=True, max_length=12)
    createdAt = DateTimeField(required=True)
    abi = BinaryField()
    updatedAt = DateTimeField()
    meta = {'db_alias': 'db1'}

#公钥表
class pub_keys(Document):
    account = StringField(required=True, max_length=12)
    permission = StringField()
    public_key = StringField(required=True, max_length=53)
    createdAt = DateTimeField(required=True)
    meta = {'db_alias': 'db1'}

#交易表
class transactions(Document):
    trx_id = StringField(required=True, max_length=64)
    expiration = StringField(max_length=30)
    ref_block_num = IntField(required=True)
    ref_block_prefix = IntField()
    max_net_usage_words = IntField()
    max_cpu_usage_ms = IntField()
    delay_sec = IntField()
    context_free_actions = BinaryField()
    actions = BinaryField()
    transaction_extensions = BinaryField()
    signatures = BinaryField()
    context_free_data = BinaryField()
    accepted = BooleanField()
    implicit = BooleanField()
    scheduled = BooleanField()
    createdAt = DateTimeField()
    block_id = StringField()
    block_num = IntField()
    signing_keys = StringField()
    irreversible = BooleanField()
    updatedAt = DateTimeField()
    meta = {'db_alias': 'db1'}

#生产者表
class producers(Document):
    ranking = IntField()
    account_name = StringField(required=True, max_length=12)
    is_active = BooleanField()
    location = StringField()
    blocks_count = IntField()
    vote_rate = StringField()
    vote = StringField()
    reward = StringField()
    url = StringField()
    total_producer_vote_weight = StringField()
    meta = {'db_alias': 'db1'}

#交易追溯表
#except字段与关键字冲突，暂时用DynamicDocument解决
class transaction_traces(DynamicDocument):
    _id = StringField()
    id = StringField(required=True, max_length=64)
    receipt = BinaryField()
    elapsed = IntField()
    net_usage = IntField()
    scheduled = BooleanField()
    action_traces = BinaryField()
    createdAt = DateTimeField()
    meta = {'db_alias': 'db1'}

#action追溯表
#except字段与关键字冲突，暂时用DynamicDocument解决
class action_traces(DynamicDocument):
    _id = StringField()
    receipt = BinaryField()
    act = BinaryField()
    context_free = BooleanField()
    elapsed = IntField()
    console = StringField()
    trx_id = StringField(max_length=64)
    block_num = IntField()
    block_time = StringField()
    producer_block_id = StringField()
    account_ram_deltas = BinaryField()
    trx_status = StringField()
    createdAt = DateTimeField()
    meta = {'db_alias': 'db1'}

#跨链交易操作表
class cross_chain_tx_actions(Document):
    cross_chain_action_no = IntField(required = True)
    trx_id = StringField(max_length=64)
    block_no = IntField()
    cross_chain_action_time = DateTimeField()
    account = StringField(required=True, max_length=12)
    status = StringField()
    action_name = StringField()
    action_data = StringField()
    meta = {'db_alias': 'db1'}
