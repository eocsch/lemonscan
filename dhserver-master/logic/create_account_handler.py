
#-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import re
import os

from odm_class import disconnect_db, connect_db

class CreateAccountHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("CreateAccountHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))

    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login CreateAccountHandler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request"
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            
            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            account = ''
            #获取请求的参数：账户名
            if 'account' in data.keys():
                account = data['account']

            if type(account).__name__ == 'str':
                matchObj = re.match( r'([a-z]|[1-5]|[.]){1,12}', account)
                if matchObj == False:
                    respon = {
                        "errno": 400, 
                        "errmsg": "Bad Request(parameter account error)"
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                if len(account) != 12:
                    respon = {
                        "errno": 400, 
                        "errmsg": "Bad Request(parameter account error)"
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return

            public_key = ''
            if 'public_key' in data.keys():
                public_key = data['public_key']

            if type(public_key).__name__ != 'str' or len(public_key) != 53:
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter public_key error)"
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            research_account = "/tkteos/home/twinkle/twinkle11/cleos.sh get account " + account
            research_result = os.system(research_account)
            if research_result == 0:
                respon = {
                    "errno": 100006, 
                    "errmsg": "Account Already Exist"
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #cmd_create_account = "/tkteos/home/twinkle/twinkle11/cleos.sh create account eosio " + account + " " + public_key + " " + public_key
            cmd_create_account = "/tkteos/home/twinkle/twinkle11/cleos.sh system newaccount eocseosioicp " + account + " " + public_key + " " + public_key + " --stake-net \"10.0000 EOS\" --stake-cpu \"10.0000 EOS\" --buy-ram \"10.0000 EOS\" -p eocseosioicp"

            create_result = os.system(cmd_create_account)
            #popen_reault = os.popen(cmd_create_account)

            if create_result == 0:
                respon = {
                    "errno": 0, 
                    "errmsg": "Success"
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            else:
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request"
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
        except Exception as e:
            print('execute create_account_handler exception...')
    get = post
