#-*-coding:utf-8-*-
import requests
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
from enum import Enum
from odm_class import cross_chain_tx_actions, disconnect_db, connect_db


class CrossChainActionHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("CrossChainActionHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))

    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login cross_chain_actions_handle success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            #获取跨链交易操作的序号
            cross_chain_action_no = 0
            if 'cross_chain_action_no' in data.keys():
                cross_chain_action_no = data['cross_chain_action_no']
                if cross_chain_action_no == None or type(cross_chain_action_no).__name__ != 'int' or (type(cross_chain_action_no).__name__ == 'int' and cross_chain_action_no < 0):
                    respon = {
                        "errno": 400, 
                        "errmsg": "Bad Request(parameter cross_chain_action_no error)", 
                        'data': None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
            if cross_chain_action_no != 0:
                cross_chain_action = cross_chain_tx_actions.objects(cross_chain_action_no = cross_chain_action_no).first()
                if cross_chain_action == None:
                    respon = {
                        "errno": 100005, 
                        "errmsg": "Action Not Exist", 
                        'data': None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
                else:
                    data = {
                        'cross_chain_action_no': cross_chain_action.cross_chain_action_no,
                        'trx_id': cross_chain_action.trx_id,
                        'block_no': cross_chain_action.block_no,
                        'cross_chain_action_time': str(cross_chain_action.cross_chain_action_time),
                        'account': cross_chain_action.account,
                        'status': cross_chain_action.status,
                        'action_name': cross_chain_action.action_name,
                        'action_data': cross_chain_action.action_data
                    }
                    respon = {
                        "errno": 0, 
                        "errmsg": "Success", 
                        'data': data
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    self.write(respon_json)
                    self.finish()
                    return
            #获取请求信息中的参数：区块信息显示的页数page_num
            page_num = 0
            if 'page_num' in data.keys():
                page_num = data['page_num']
            #返回请求参数错误的响应：page_num不是大于0的int型数
            if page_num == None or type(page_num).__name__ != 'int' or (type(page_num).__name__ == 'int' and page_num < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_num error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #获取请求信息中的参数：每页显示的区块个数page_size
            page_size = 0
            if 'page_size' in data.keys():
                page_size = data['page_size']
            #返回请求参数错误的响应：page_size不是大于0的int型数
            if page_size == None or type(page_size).__name__ != 'int' or (type(page_size).__name__ == 'int' and page_size < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter page_size error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            rows = []
            actions_count = cross_chain_tx_actions.objects().all().count()

            if page_num == 1:
                actions_search = cross_chain_tx_actions.objects.all().order_by('-cross_chain_action_no').limit(page_size).skip((page_num - 1) * page_size)
                print('ok1')
                if actions_search.first() == None:
                    respon = {
                        "errno": 100005, 
                        "errmsg": "Action Not Exist", 
                        'data': None
                    }
                    respon_json = tornado.escape.json_encode(respon)
                    print(respon_json)
                    self.write(respon_json)
                    self.finish()
                    return
                for action in actions_search:
                    action_info = {
                        "cross_chain_action_no": action.cross_chain_action_no,
                        "trx_id": action.trx_id,
                        "block_no": action.block_no,
                        "cross_chain_action_time": str(action.cross_chain_action_time),
                        "account": action.account,
                        "action_name": action.action_name
                    }
                    rows.append(action_info)
                print('ok2')
            else:
                start_action_no = actions_count - (page_num - 1) * page_size
                now_action_no = start_action_no
                while now_action_no < (actions_count + 1) and now_action_no > (start_action_no - page_size) and now_action_no > 0:
                    action_db = cross_chain_tx_actions.objects(cross_chain_action_no = now_action_no).first()
                    action_info = {
                        "cross_chain_action_no": action_db.cross_chain_action_no,
                        "trx_id": action_db.trx_id,
                        "block_no": action_db.block_no,
                        "cross_chain_action_time": str(action_db.cross_chain_action_time),
                        "account": action_db.account,
                        "action_name": action_db.action_name
                    }
                    rows.append(action_info)
                    now_action_no -= 1

            #返回查询到的生产者信息
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": {
                    "total": actions_count,
                    "actions_info": rows
                }
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print(str(e))
            print('CrossChainActionHandler exception...')
    get = post
