#-*-coding:utf-8-*-

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import json
import ast
from odm_class import blocks, disconnect_db, connect_db


class BlockDetailHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        try:
            print("setting headers!!!", self.request.headers.keys())        
            self.set_header("Access-Control-Allow-Headers", "x-requested-with")
            self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
            self.set_header("Access-Control-Allow-Credentials", "true")
            self.set_header("Access-Control-Allow-Headers", "Content-Type")
            if 'Access-Control-Allow-Origin' in self.request.headers.keys():
                orgin = self.request.headers["Access-Control-Allow-Origin"]    
                self.set_header("Access-Control-Allow-Origin", orgin)
                print("Access-Control-Allow-Origin ===")
            elif 'Origin' in self.request.headers.keys():
                orgin = self.request.headers["Origin"]
                self.set_header("Access-Control-Allow-Origin", orgin)
            else:
                print(" not found Access-Control-Allow-Origin and Origin")
                self.set_header("Access-Control-Allow-Origin", "*")
        except Exception as e:
            print("set default headers error is %s"  %(str(e) ) )
    
    def options(self):
        try:
            # no body
            #self.set_status(200)
            print("BlockDetailHandler options")
            pass
            #self.finish()
        except Exception as e:
            print("options error is %s" %(str(e)))
    
    def post(self):
        try:
            data=json.loads(self.request.body.decode('utf-8'))
            print("call Login block_detail_handler success!!!")
            #请求信息的类型错误
            if type(data).__name__ != 'dict':
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #获取数据库名称
            db_name = ''
            if 'db_name' in data.keys():
                db_name = data['db_name']

            if db_name == None or db_name == '' or type(db_name) != type(''):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request(parameter db_name error)", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return

            #数据库连接重定向
            disconnect_db()
            connect_db(db_name)

            #获取请求的参数：区块num
            block_num = 0
            if 'block_num' in data.keys():
                block_num = data['block_num']
            #返回请求参数错误的响应：不是大于0的int型数
            if block_num == None or type(block_num).__name__ != 'int' or (type(block_num).__name__ == 'int' and block_num < 1):
                respon = {
                    "errno": 400, 
                    "errmsg": "Bad Request", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            #根据请求的参数（区块num）在表中查询区块的信息
            #block_info = session.query(Block).filter(Block.block_num == block_num).first()
            block_info = blocks.objects(block_num = block_num).first()
            #没有查询到区块信息，返回100001的错误码
            if block_info == None:
                respon = {
                    "errno": 100001, 
                    "errmsg": "Block Not Exist", 
                    'data': None
                }
                respon_json = tornado.escape.json_encode(respon)
                self.write(respon_json)
                self.finish()
                return
            block_dict = block_info.block
            #区块信息
            info = {
                "block_num": block_num, 
                "confirmed": block_dict['confirmed'],
                "id": block_info.block_id, 
                "new_producers": block_dict['new_producers'],
                "previous": block_dict['previous'],
                "producer": block_dict['producer'],
                "producer_signature": block_dict['producer_signature'],
                "timestamp": block_dict['timestamp'],
                "transaction_mroot": block_dict['transaction_mroot'],
                "action_mroot": block_dict['action_mroot'],
                "transactions": block_dict['transactions']
            }
            respon = {
                "errno": 0, 
                "errmsg": "Success", 
                "data": info
            }
            respon_json = tornado.escape.json_encode(respon)
            self.write(respon_json)
            self.finish()
        except Exception as e:
            print('get_argument block_detail_handler exception...')

    get = post