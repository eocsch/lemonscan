#-*-coding:utf-8-*-
import os
import sys
from base_info_handler import BaseInfoHandler
from block_info_handler import BlockInfoHandler
from block_detail_handler import BlockDetailHandler
from transaction_detail_handler import TransactionDetailHandler
from search_handler import SearchlHandler
from producers_handler import ProducersHandler
from cross_chain_action_handler import CrossChainActionHandler
from create_account_handler import CreateAccountHandler
from account_actions_handler import AccountActionsHandler

callhandlers=[
              #全网预览信息
              (r"/v1/eos/get_base_info", BaseInfoHandler),
              #最新区块列表
              (r"/v1/eos/get_block_info", BlockInfoHandler),
              #区块详情
              (r"/v1/eos/get_block_detail", BlockDetailHandler),
              #交易详情
              (r"/v1/eos/get_transaction_detail", TransactionDetailHandler),
              #用户检索：1.pub_key(公钥), 2.account(账户), 3.transaction(交易), 4.block(区块)
              (r"/v1/eos/get_search", SearchlHandler),
              #超级节点/生产者信息
              (r"/v1/eos/get_block_producers_info", ProducersHandler),
              #跨链交易操作信息
              (r"/v1/eos/get_cross_chain_action_info", CrossChainActionHandler),
              #创建账户
              (r"/v1/eos/create_account", CreateAccountHandler),
              #查询账户的actions
              (r"/v1/eos/get_account_actions", AccountActionsHandler),
            ]
