#-*-coding:utf-8-*-
import sys 
import os
import requests
import json
from pymongo import MongoClient
from mongoengine import *
from datetime import datetime
import time
curdir = os.path.dirname(__file__)
logicdirelative = os.path.join(curdir,'../logic')
#print(logicdirelative)
logicdir = os.path.abspath(logicdirelative)
#print(logicdir)
sys.path.append(logicdir)
from odm_class import blocks
from odm_class import producers
from odm_class import transactions
from odm_class import transaction_traces
from odm_class import cross_chain_tx_actions
from odm_class import connect_db, disconnect_db
from odm_class import mysql_addr,mysql_port,mysql_username,mysql_dbname,mysql_pswd
import pymysql

#连接mysql
def connectmysql():
    try:
        # Author:dancheng
        conn = pymysql.connect(host=mysql_addr, port=mysql_port, user=mysql_username, passwd=mysql_pswd, db=mysql_dbname)
        return conn
    except Exception as e:
        print(str(e))

#断开连接
def disconmysql(conn):
    try:
        conn.close()
    except Exception as e:
        print(str(e))

#获取节点信息[('192.168.1.1:9909','EOS1'),('192.168.1.2:8898','EOS2')]
def get_nodedata(conn):
    try:
        print("BP MESSAGE BEGIN GET NODE????????????????????")
        datas=[]
        cur = conn.cursor()
        rescount = cur.execute('select httpserveraddress, db_name from base_node')
        print("BP MESSAGE select httpserveraddress, db_name from base_node")
        #print(rescount)
        results = cur.fetchall()
        for row in results:
            datas.append((row[0],row[1]))    
        cur.close()
        return datas
    except Exception as e:
        print(str(e))

# 更新mongodb表
def update_producerdb(datas):
    try:
        for data in datas:
            node_addr = data[0]
            db_name = data[1]
            disconnect_db()
            connect_db(db_name)
            rows = []
            posturls='http://' + node_addr + '/v1/chain/get_producers'
            payload = {'scope':'eosio', 'code':'eosio', 'table':'global', 'json':True}
            header = {'Content-Type': 'application/json'}    ## headers中添加上content-type这个参数，指定为json格式
            response = requests.post(posturls, headers=header, data=json.dumps(payload), timeout=5)    ## post的时候，将data字典形式的参数用json包转换成json格式。
           
            res_json = response.json()
            print(res_json)
            if res_json == None:
                print("res_json is none")
                return
            if 'code' in res_json.keys() and res_json['code'] != 200:
                print("code not in res_json or post error")
                return
            if 'rows' not in res_json.keys():
                print("rows not in res_json")
                return
            list_producers = res_json['rows']
            
            ranking_no = 1
            for list_producer in list_producers:
                producerinfo = producers.objects(account_name = list_producer['owner']).first()
                if producerinfo == None:
                    producerinfo = producers(account_name = list_producer['owner'])
                producerinfo.ranking = ranking_no
                producerinfo.account_name = list_producer['owner']
                producerinfo.is_active = True
                producerinfo.location = "中国"
                producerinfo.blocks_count = blocks.objects(__raw__={'block.producer': list_producer['owner']}).count()
                if float(res_json['total_producer_vote_weight']) != 0:
                    producerinfo.vote_rate = '{:.2%}'.format(float(list_producer['total_votes'])/float(res_json['total_producer_vote_weight']))
                else:
                    producerinfo.vote_rate = '0.00'
                producerinfo.vote = '%.2f' % (float(list_producer['total_votes'])/10000)
                producerinfo.reward = "1345/3766"
                producerinfo.url = list_producer['url']
                producerinfo.total_producer_vote_weight = res_json['total_producer_vote_weight']
                producerinfo.save()
                ranking_no +=1
               
    except Exception as e:
        print("catch exception")
        print(str(e))



def time_decorator(duration):
    def outer_wrapper(func):
        def innerWrapper(*args, **kw):
            while True:
                print(datetime.now().strftime("%Y-%m-%d  %H:%M:%S"))
                func()
                time.sleep(duration)
        return innerWrapper
    return outer_wrapper
    
@time_decorator(duration=1800)
def update_mongodb():
    print("BP MESSAGE SERVER BEGIN")
    conn = connectmysql()
    print("BP MESSAGE connectmysql")
    print("................")
    datas = get_nodedata(conn)
    print("BP MESSAGE get_nodedata")
    print(datas)
    update_producerdb(datas)
    disconmysql(conn)



if __name__ == '__main__':
    try:
        update_mongodb()
    except Exception as e: 
        print(str(e))
        

